<?php
	/**
	 * @author    MohammadMahdi Afshar <me@reloadlife.me>
	 * @copyright 2018-2019 ReloadLife <me@reloadlife.me>
	 * @license   https://opensource.org/licenses/AGPL-3.0 AGPLv3
	 *
	 */
	namespace MadeLineProtoFA;

	require "vendor/autoload.php";

	header("content-type: application/json");
	$token = parse_ini_file('config.ini')['token'];
	if ( isset($_GET['token'] )) {
		$token = $_GET['token'];
	}
	$Bot = new Bot($token);

	$Bot->handle();
