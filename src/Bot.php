<?php
	/**
	 * @author    MohammadMahdi Afshar <me@reloadlife.me>
	 * @copyright 2018-2019 ReloadLife <me@reloadlife.me>
	 * @license   https://opensource.org/licenses/AGPL-3.0 AGPLv3
	 *
	 */
	namespace MadeLineProtoFA;

	use TelegramBotPHP\getUpdate;
	use TelegramBotPHP\methods;
	use TelegramBotPHP\types\ChatMember;

	class Bot {
		protected $Token;
		protected $hooked;

		protected $tg;
		protected $update;

		public function __construct ( $Token ) {
			$this->Token = $Token;
			$this->update = new getUpdate();
			$this->tg = new methods($Token);
		}
		public function hookAnswer ( $method, $parameters ) {
			if ( $this->hooked ) {
				try {
					return $this -> tg -> __call( $method, [$parameters] );
				} catch ( \Exception $exception ) {}
			}
			$parameters["method"] = $method;
			echo json_encode( array_filter( $parameters ) );
			$this -> hooked = true;
			fastcgi_finish_request();
			set_time_limit( -1 );
			return true;
		}

		public function handle () {
			if ( $this->update->chat->type && $this->update->chat->type == 'supergroup' ) {
				return $this->inSuperGroup();
			}
			if ( $this->update->chat->type && $this->update->chat->type == 'private' ) {
				return $this->inMessage();
			}
			return 'unSupported';
		}

		protected function inMessage () {
			if ( $this->update->forward_from ) {
				return $this->hookAnswer('sendMessage', [
					'chat_id' => $this->update->chat->id,
					'parse_mode' => "markdown",
					'reply_to_message_id' => $this->update->message_id,
					'text' => "*ID*: `{$this->update->forward_from->id}`"
						. "\n*Name*: {$this->tg->parse_markdown($this->update->forward_from->first_name)}"
						. ($this->update->forward_from->username?"\n*Username*: @{$this->tg->parse_markdown($this->update->forward_from->username)}":'')
				]);
			}

			if ( $this->update->forward_from_chat ) {
				return $this->hookAnswer('sendMessage', [
					'chat_id' => $this->update->chat->id,
					'parse_mode' => "markdown",
					'reply_to_message_id' => $this->update->message_id,
					'text' => "*ID*: `{$this->update->forward_from_chat->id}`"
						. "\n*Name*: {$this->tg->parse_markdown($this->update->forward_from_chat->first_name)}"
						. ($this->update->forward_from_chat->username?"\n*Username*: @{$this->tg->parse_markdown($this->update->forward_from_chat->username)}":'')
				]);
			}
			return $this->hookAnswer('sendMessage', [
				'chat_id' => $this->update->chat->id,
				'parse_mode' => "markdown",
				'reply_to_message_id' => $this->update->message_id,
				'text' => "*Hello {$this->tg->parse_markdown($this->update->from->first_name)} !*"
					. "\nA *Free* Group *Manager Robot*"
					. "\n_In SuperGroup Commands:_"
					. "\n`/id [reply]`: *shows Group ID, YourID and [Replied UserID]*"
					. "\n_In Pv Commands:_"
					. "\n`[Forward Message]`: *Shows Message Sender's information*"
					. "\n`/who (id)`: *Mentions ID [if exist in my DataPeer]*"
					. "\n[Source Code on GitLab](https://gitlab.com/ReloadLife/MadeLineProtoFA)"
			]);
		}

		protected function inSuperGroup () {
			if ( $this->update->new_chat_members ) {
				return $this->hookAnswer('deleteMessage', ['chat_id' => $this->update->chat->id, 'message_id' => $this->update->message_id ]);
			}
			if ( $this->update->caption_entities )
				$this->update->entities = $this->update->caption_entities;

			$isLink = false;
			if ($this->update->entities) {
				foreach ($this -> update -> entities as $messageEntity) {
					if ($messageEntity -> type == 'url' || $messageEntity -> type == 'text_link') {
						$isLink = true;
					}
				}
			}

			if ( $this->update->forward_from && $isLink ) {
				return $this->hookAnswer('deleteMessage', ['chat_id' => $this->update->chat->id, 'message_id' => $this->update->message_id ]);
			}

			if ( $this->update->text ) {
				$text = $this->update->text;

				if (strstr($text, 'مدلاین') and !strstr($text, 'میدلاین')) {
				    return $this->hookAnswer('sendMessage', [
						'chat_id' => $this->update->chat->id,
						'reply_to_message_id' => $this->update->message_id,
						'text' => "مِیدلاین *"
					]);
				}
				
				
				if ( strstr(strtolower($text), 'mobogram.apk') ) {
				    return $this->hookAnswer('deleteMessage', ['chat_id' => $this->update->chat->id, 'message_id' => $this->update->message_id ]);
				}

				if ( (strstr($text, 'اوبنتو')||strstr($text, 'ابونتو')) and !strstr($text, 'اوبونتو')) {
				    return $this->hookAnswer('sendMessage', [
						'chat_id' => $this->update->chat->id,
						'reply_to_message_id' => $this->update->message_id,
						'text' => "اوبونتو *"
					]);
				}


				if ( $text == '/id' ) {
					if ( $this->update->reply_to_message ) {
						return $this->hookAnswer('sendMessage', [
							'chat_id' => $this->update->chat->id,
							'parse_mode' => "markdown",
							'reply_to_message_id' => $this->update->reply_to_message->message_id,
							'text' => "*ChatID:* `{$this->update->chat->id}`"
								."\n*ReplyUserID:* [{$this->update->reply_to_message->from->id}](tg://user?id={$this->update->reply_to_message->from->id})"
								."\n*YourID:* [{$this->update->from->id}](tg://user?id={$this->update->from->id})"
						]);
					}
					return $this->hookAnswer('sendMessage', [
						'chat_id' => $this->update->chat->id,
						'parse_mode' => "markdown",
						'reply_to_message_id' => $this->update->message_id,
						'text' => "*ChatID:* `{$this->update->chat->id}`\n*YourID:* [{$this->update->from->id}](tg://user?id={$this->update->from->id})"
					]);
				}

				if ($text == '/report' && $this -> update -> reply_to_message) {
					fastcgi_finish_request();
					$Admins = [];
					$admins = $this->tg->getChatAdministrators(['chat_id' => $this->update->chat->id])->getRawBody()->result;
					foreach ($admins as $admin ) {
						$Admins[] = $admin->user->id;
					}
					foreach (array_unique($Admins) as $adminId) {
						try {
							$this->tg->empty();
							$idMid = $this -> tg -> forwardMessage( [
								'chat_id' => $adminId, 'from_chat_id' => $this -> update -> chat -> id,
								'message_id' => $this -> update -> reply_to_message -> message_id,
							] );
							$textReport = "*Reported By* [{$this->update->from->id}](tg://user?id={$this->update->from->id})" . "\n*inChat:* {$this->tg->parse_markdown($this->update->chat->title)}";
							$this->tg->empty();
							$this -> tg -> sendMessage( [
								'chat_id' => $adminId, 'reply_to_message_id' => $idMid -> message_id,
								"text" => $textReport, 'parse_mode' => 'markdown'
							] );
						}catch (\Exception $e) {error_log($e);}
					}
					return $this -> tg -> deleteMessage( [
						'chat_id' => $this -> update -> chat -> id, 'message_id' => $this -> update -> message_id,
					] );
				}

				if ($text == '/link' ) {
					$link = $this -> tg -> exportChatInviteLink( [ 'chat_id' => $this -> update -> chat -> id ] );
					return $this -> hookAnswer( 'sendMessage', [
						'parse_mode' => "markdown",
						'reply_to_message_id' => $this->update->message_id,
						'chat_id' => $this -> update -> chat -> id, 'text' => $this->tg->parse_markdown($link->getRawBody()->result),
					] );
				}

				if ( $text == '/help' ) {
					return $this->hookAnswer('sendMessage', [
						'chat_id' => $this->update->chat->id,
						'parse_mode' => "markdown",
						'reply_to_message_id' => $this->update->message_id,
						'text' => "\n_In SuperGroup Commands:_"
							. "\n`/id [reply]`: *shows Group ID, YourID and [Replied UserID]*"
							. "\n`/link`: *shows Group link ( expires in 10min )*"
							. "\n`/report (reply)`: *reports message to chat admins*"
							. "\n_In Pv Commands:_"
							. "\n`[Forward Message]`: *Shows Message Sender's information*"
							. "\n`/who (id)`: *Mentions ID [if exist in my DataPeer]*"
							. "\n[Source Code on GitLab](https://gitlab.com/ReloadLife/MadeLineProtoFA)"
					]);
				}
			}
			return true;
		}
	}